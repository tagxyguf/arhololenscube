﻿using Microsoft.MixedReality.Toolkit.Input;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeManager : BaseInputHandler, IMixedRealityInputHandler
{
    public void OnInputDown(InputEventData eventData)
    {
        GetComponent<MeshRenderer>().material.color = Color.green;
    }

    public void OnInputUp(InputEventData eventData)
    {
        GetComponent<MeshRenderer>().material.color = Color.red;
    }

    protected override void RegisterHandlers()
    {
        throw new System.NotImplementedException();
    }

    protected override void UnregisterHandlers()
    {
        throw new System.NotImplementedException();
    }
}
