# AR HoloLens Project simple example for showing a cube

AR HoloLens Project is a simple example in 3 stages:
1) simple spawning of a cube directly in front of the user
2) simple spawning of a colored cube and rotating it in front of the user
3) MRTK2 based example spawning and rotating a cube in front of the user, the user can airtap or touch the cube to change its color

The result should look somehow like this in the editor:

![simple cube spawning](images/cubespawning.png)
![simple cube rotating](images/cuberotating.png)
![simple cube MRTK2 interaction](images/mrtk2interaction.png)

## Project setup
Unity Version: 2019.3.15f1
Checkout the project and be aware that you do not need MRTK2 for the first 2 stages. For stage 3 make sure to install MRTK2 unity plugin.

### HoloLens 1
MRTK2 should also work on HoloLens 1. As I am using only a simple trigger interaction it will work based on gaze and airtap with HoloLens 1.

### HoloLens 2
With HoloLens 2 it will be possible to directly touch the cube with your finger to change its color.

## Contributing
It is not planned to have contributions here.

## License
[MIT](https://choosealicense.com/licenses/mit/)Setup Manual